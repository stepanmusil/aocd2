input_name = "input.txt"

substitution = {"Y": "B", "X": "A", "Z": "C"}
score = {"A": 1, "B": 2, "C": 3}
victory = ["AB", "BC", "CA"]

def round_count(input_name):
    result = 0
    with open(input_name, "r") as f:
        for line in f:
            opponent, coded_player = line.split()
            player = substitution[coded_player]
            round_choice = opponent + player
            if opponent == player:
                result += 3
            elif round_choice in victory:
                result += 6
            result += score[player]
    return result

res = round_count(input_name)
print(res)